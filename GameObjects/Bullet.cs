﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct2D1;

namespace GameObjects
{
    public class Bullet
    {
        public Vector2 position;
        public int direction;

        public Bullet(Vector2 position, int direction)
        {
            this.position = position;
            this.direction = direction;
        }
    }
}
