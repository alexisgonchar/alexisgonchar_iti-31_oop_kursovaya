﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct2D1;

namespace GameObjects
{
    public class Rock
    {
        public int scale;
        public Vector2 position;
        public Rock(Vector2 position)
        {
            this.position = position;
            scale = 70;
        }
    }
}
