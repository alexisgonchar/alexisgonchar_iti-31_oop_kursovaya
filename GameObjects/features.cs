﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Linq;

namespace GameObjects
{
    public static class Features
    {
        public static void swap(ref int a, ref int b)
        {
            int c = a;
            a = b;
            b = c;
        }

        //Общая проверка на коллизию
        public static bool CollisionRect(int r1x, int r1y, int r2x, int r2y, int r1w, int r1h, int r2w, int r2h)
        {
            if (r1x + r1w >= r2x &&
                r1x <= r2x + r2w &&
                r1y + r1h >= r2y &&
                r1y <= r2y + r2h)
            {
                return true;
            }
            return false;
        }

        public static int[] getShipParameters(int index)
        {
            return File.ReadAllLines("config")[index].Split(' ').Select(n => Convert.ToInt32(n)).ToArray();
        }
    }
}
