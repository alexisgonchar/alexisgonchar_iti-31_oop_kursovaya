﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameObjects
{
    public class AuroraShip : ShipDecorator
    {
        int[] parameters;
        public AuroraShip(Ship player) : base(player.position, player)
        {
            parameters = Features.getShipParameters(0);
        }

        public override void takeDamage(int damage)
        {
            Health -= damage;
        }

        public override int getDamage()
        {
            return player.getDamage() + parameters[3];
        }

        public override float getDamageAbsorption()
        {
            return player.getDamageAbsorption() + parameters[4]/100.0f;
        }

        public override int getSpeed()
        {
            return player.getSpeed() + parameters[0];
        }

        public override int getSpeedBullet()
        {
            return player.getSpeedBullet() + parameters[1];
        }

        public override int getWeaponsReload()
        {
            return player.getWeaponsReload() + parameters[2];
        }
    }
}
