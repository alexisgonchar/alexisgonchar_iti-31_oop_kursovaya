﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;

namespace GameObjects
{
    public abstract class ShipDecorator : Ship
    {
        protected Ship player;
        public ShipDecorator(Vector2 position, Ship player) : base(position)
        {
            this.player = player;
        }
    }
}
