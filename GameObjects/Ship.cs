﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct2D1;

namespace GameObjects
{
    public abstract class Ship
    {
        public bool isAlive;
        public int Health { get { return health; }
            set
            {
                if (value < 0) health = 0;
                else health = value;
            }
        }
        private int health;
        public int direction;
        public int IsReload;
        public Vector2 position;
        public int height;
        public int width;
        public List<Bullet> bullets = new List<Bullet>();
        public int speedBullet;
        public bool weaponsMode;
        public int speed;
        public int weaponsReload;
        public int damage;
        public float damageAbsorption;

        public Ship(Vector2 position)
        {
            weaponsMode = true;
            isAlive = true;
            Health = 1000;
            IsReload = 0;
            this.position = position;
            height = 55;
            width = 110;
            direction = 1;
            damageAbsorption = 0;
        }

        public abstract int getSpeed();
        public abstract int getSpeedBullet();
        public abstract int getDamage();
        public abstract int getWeaponsReload();
        public abstract float getDamageAbsorption();
        public abstract void takeDamage(int damage);
    }
}
