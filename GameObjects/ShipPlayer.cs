﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;

namespace GameObjects
{
    public class ShipPlayer : Ship
    {
        public ShipPlayer(Vector2 position) : base(position)
        {
        }

        public override int getDamage()
        {
            return damage;
        }

        public override void takeDamage(int damage)
        {
            Health -= damage;
        }

        public override float getDamageAbsorption()
        {
            return damageAbsorption;
        }

        public override int getSpeed()
        {
            return speed;
        }

        public override int getSpeedBullet()
        {
            return speedBullet;
        }

        public override int getWeaponsReload()
        {
            return weaponsReload;
        }
    }
}
