﻿using SharpDX;
using SharpDX.Direct2D1;
using GameControl;
using System.Windows.Input;
using System.Collections.Generic;
using GameObjects;

namespace SeaBattleGame
{
    class Renderer : Direct2DComponent
    {

        Ship player1;
        Ship player2;
        private Vector2 position1 = new Vector2(200, 200);
        private Vector2 position2 = new Vector2(420, 200);
        private SolidColorBrush color1;
        private SolidColorBrush color2;
        private SolidColorBrush colorHealth;
        private SolidColorBrush colorHealthBg;
        private SolidColorBrush colorReload;
        private SolidColorBrush colorReloadBg;
        SharpDX.DirectWrite.Factory factory;
        private List<Rock> rocks;

        Bitmap shipLeft, shipRight, shipUp, shipDown, bulletBmp, seaBmp, flagpl1, flagpl2, rockBmp;

        Vector4 borders;

        public Renderer(Dictionary<string, int> dc)
        {
            generatePlayer(dc);
        }

        public void generatePlayer(Dictionary<string, int> dc)
        {
            int ship1, ship2;
            ship1 = dc["Ship1"];
            ship2 = dc["Ship2"];
            player1 = new ShipPlayer(position1);
            player2 = new ShipPlayer(position2);
            switch (ship1)
            {
                case 0:
                    player1 = new AuroraShip(player1);
                    break;
                case 1:
                    player1 = new FlyingDutchmanShip(player1);
                    break;
                case 2:
                    player1 = new ForwardShip(player1);
                    break;
                case 3:
                    player1 = new GhostShip(player1);
                    break;
                case 4:
                    player1 = new PiligrimShip(player1);
                    break;
            }
            switch (ship2)
            {
                case 0:
                    player2 = new AuroraShip(player2);
                    break;
                case 1:
                    player2 = new FlyingDutchmanShip(player2);
                    break;
                case 2:
                    player2 = new ForwardShip(player2);
                    break;
                case 3:
                    player2 = new GhostShip(player2);
                    break;
                case 4:
                    player2 = new PiligrimShip(player2);
                    break;
            }
        }

        protected override void InternalInitialize()
        {
            base.InternalInitialize();
            factory = new SharpDX.DirectWrite.Factory();

            shipLeft = BitmapWorker.LoadFromFile(RenderTarget2D, @"resources\shipLeft.png");
            shipRight = BitmapWorker.LoadFromFile(RenderTarget2D, @"resources\shipRight.png");
            shipUp = BitmapWorker.LoadFromFile(RenderTarget2D, @"resources\shipUp.png");
            shipDown = BitmapWorker.LoadFromFile(RenderTarget2D, @"resources\shipDown.png");
            bulletBmp = BitmapWorker.LoadFromFile(RenderTarget2D, @"resources\bullet.png");
            seaBmp = BitmapWorker.LoadFromFile(RenderTarget2D, @"resources\sea.jpg");
            flagpl1 = BitmapWorker.LoadFromFile(RenderTarget2D, @"resources\player1.png");
            flagpl2 = BitmapWorker.LoadFromFile(RenderTarget2D, @"resources\player2.png");
            rockBmp = BitmapWorker.LoadFromFile(RenderTarget2D, @"resources\rock.png");

            color1 = new SolidColorBrush(RenderTarget2D, new Color(0, 0, 0));
            color2 = new SolidColorBrush(RenderTarget2D, new Color(146, 78, 0));
            colorHealth = new SolidColorBrush(RenderTarget2D, new Color(0, 255, 0));
            colorHealthBg = new SolidColorBrush(RenderTarget2D, new Color(255, 0, 0));
            colorReload = new SolidColorBrush(RenderTarget2D, new Color(0, 0, 255));
            colorReloadBg = new SolidColorBrush(RenderTarget2D, new Color(255, 255, 255));

            borders = new Vector4(30, 0, 704, 570);

            generateRocks();
        }

        private void generateRocks()
        {
            rocks = new List<Rock>();
            rocks.Add(new Rock(new Vector2(280, 180)));
            rocks.Add(new Rock(new Vector2(420, 380)));
            rocks.Add(new Rock(new Vector2(80, 320)));
        }

        protected override void InternalUninitialize()
        {
            Utilities.Dispose(ref color1);
            Utilities.Dispose(ref color2);

            base.InternalUninitialize();
        }


        //
        protected override void Render()
        {
            UpdatePosition();
            //Фон
            RenderTarget2D.Clear(new Color(255, 255, 255));
            RenderTarget2D.DrawBitmap(seaBmp, new RectangleF(0, 0, 733, 703), 1.0f, BitmapInterpolationMode.Linear);

            //Полет пули и проверка на удар
            BulletsMove(player1, player2);

            BulletsMove(player2, player1);

            //Отрисовка игроков
            Bitmap ship = chooseDirectionShip(player1.direction);
            RenderTarget2D.DrawBitmap(ship, new RectangleF(player1.position.X - player1.width / 2, player1.position.Y - player1.height / 2, player1.width, player1.height), 1.0f, BitmapInterpolationMode.Linear);
            RenderTarget2D.DrawBitmap(flagpl1, new RectangleF(player1.position.X - 14, player1.position.Y - 14, 28, 28), 1.0f, BitmapInterpolationMode.Linear);
            ship = chooseDirectionShip(player2.direction);
            RenderTarget2D.DrawBitmap(ship, new RectangleF(player2.position.X - player2.width / 2, player2.position.Y - player2.height / 2, player2.width, player2.height), 1.0f, BitmapInterpolationMode.Linear);
            RenderTarget2D.DrawBitmap(flagpl2, new RectangleF(player2.position.X - 14, player2.position.Y - 14, 28, 28), 1.0f, BitmapInterpolationMode.Linear);

            //Полоса здоровья первого игрока
            RenderTarget2D.FillRectangle(new RectangleF(40, 20, 100, 10), colorHealthBg);
            RenderTarget2D.FillRectangle(new RectangleF(40, 20, (int)(player1.Health * (100.0 / 1000)), 10), colorHealth);

            //Полоса здоровья второго игрока
            RenderTarget2D.FillRectangle(new RectangleF(590, 20, 100, 10), colorHealthBg);
            RenderTarget2D.FillRectangle(new RectangleF(590, 20, (int)(player2.Health * (100.0 / 1000)), 10), colorHealth);

            //Полоса перезарядки первого игрока
            RenderTarget2D.FillRectangle(new RectangleF(40, 40, 100, 10), colorReloadBg);
            RenderTarget2D.FillRectangle(new RectangleF(40, 40, (int)((player1.getWeaponsReload() - player1.IsReload) * (100.0 / player1.getWeaponsReload())), 10), colorReload);

            //Полоса перезарядки второго игрока
            RenderTarget2D.FillRectangle(new RectangleF(590, 40, 100, 10), colorReloadBg);
            RenderTarget2D.FillRectangle(new RectangleF(590, 40, (int)((player2.getWeaponsReload() - player2.IsReload) * (100.0 / player2.getWeaponsReload())), 10), colorReload);

            drawRocks();

            //GameOver
            if (!player1.isAlive || !player2.isAlive)
            {
                var textFormat = new SharpDX.DirectWrite.TextFormat(factory, "Algerian", 70);
                RenderTarget2D.DrawText("GAME OVER", textFormat, new RectangleF(170, 220, 600, 400), color1);
            } 
        }

        private void drawRocks()
        {
            foreach(Rock rock in rocks)
            {
                RenderTarget2D.DrawBitmap(rockBmp, new RectangleF(rock.position.X, rock.position.Y, rock.scale, rock.scale), 1.0f, BitmapInterpolationMode.Linear);
            }
        }

        private void swapDirection(Ship pl, int direction)
        {
            if((direction == 1 || direction == 3) && pl.height > pl.width)
            {
                Features.swap(ref pl.width, ref pl.height);
            }
            else if ((direction == 2 || direction == 4) && pl.height < pl.width)
            {
                Features.swap(ref pl.width, ref pl.height);
            }
            pl.direction = direction;
        }

        private Bitmap chooseDirectionShip(int direction)
        {
            switch (direction)
            {
                case 1: return shipLeft;
                case 2: return shipUp;
                case 3: return shipRight;
                case 4: return shipDown;
            }
            return shipLeft;
        }

        private bool CollisionRockPl(Ship pl)
        {
            int r1x, r1y, r1w, r1h;
            r1x = (int)(pl.position.X - pl.width / 2);
            r1y = (int)(pl.position.Y - pl.height / 2);
            r1w = pl.width;
            r1h = pl.height;
            foreach (Rock rock in rocks)
            {
                if (Features.CollisionRect(r1x, r1y, (int)rock.position.X, (int)rock.position.Y, r1w, r1h, rock.scale, rock.scale))
                {
                    return true;
                }
            }
            return false;
        }

        //Проверка на коллизию с пулей
        private bool CollisionBullet(Ship pl, Bullet bl)
        {
            int r1x, r1y, r2x, r2y, r1w, r1h, r2w, r2h;
            r1x = (int)(pl.position.X - pl.width / 2);
            r1y = (int)(pl.position.Y - pl.height / 2);
            r2x = (int)bl.position.X;
            r2y = (int)bl.position.Y;
            r1w = player1.width;
            r1h = player1.height;
            r2w = 10;
            r2h = 10;
            return Features.CollisionRect(r1x, r1y, r2x, r2y, r1w, r1h, r2w, r2h);
        }

        //Проверка на коллизию между игроками
        private bool CollisionCheck()
        {
            int r1x, r1y, r2x, r2y, r1w, r1h, r2w, r2h;
            r1x = (int)(player1.position.X - player1.width / 2);
            r1y = (int)(player1.position.Y - player1.height / 2);
            r2x = (int)(player2.position.X - player2.width / 2);
            r2y = (int)(player2.position.Y - player2.height / 2);
            r1w = player1.width;
            r1h = player1.height;
            r2w = player2.width;
            r2h = player2.height;
            return Features.CollisionRect(r1x, r1y, r2x, r2y, r1w, r1h, r2w, r2h);
        }

        private bool borderCheck(Vector2 position)
        {
            return (borders.X < position.X && borders.Y < position.Y && borders.Z > position.X && borders.W > position.Y);
        }

        private void movePlayer(Key key, Ship pl, Vector2 vec, int direction)
        {
            if (Keyboard.IsKeyDown(key))
            {
                pl.position += vec;
                swapDirection(pl, direction);
                if (CollisionCheck() || CollisionRockPl(pl) || !borderCheck(pl.position))
                    pl.position -= vec;
            }
        }

        private void UpdatePosition()
        {
            if(player1.isAlive && player2.isAlive)
            {
                //Движение первого игрока
                movePlayer(Key.W, player1, new Vector2(0, -player1.getSpeed()), 2);
                movePlayer(Key.S, player1, new Vector2(0, player1.getSpeed()), 4);
                movePlayer(Key.A, player1, new Vector2(-player1.getSpeed(), 0), 1);
                movePlayer(Key.D, player1, new Vector2(player1.getSpeed(), 0), 3);

                //Движение второго игрока
                movePlayer(Key.Up, player2, new Vector2(0, -player2.getSpeed()), 2);
                movePlayer(Key.Down, player2, new Vector2(0, player2.getSpeed()), 4);
                movePlayer(Key.Left, player2, new Vector2(-player2.getSpeed(), 0), 1);
                movePlayer(Key.Right, player2, new Vector2(player2.getSpeed(), 0), 3);

                //Смена режима стрельбы
                if (Keyboard.IsKeyDown(Key.R) && player1.IsReload == 0)
                {
                    player1.weaponsMode = !player1.weaponsMode;
                    player1.IsReload = player1.getWeaponsReload();
                }
                if (Keyboard.IsKeyDown(Key.RightShift) && player2.IsReload == 0)
                {
                    player2.weaponsMode = !player2.weaponsMode;
                    player2.IsReload = player2.getWeaponsReload();
                }

                //Стрельба первого игрока
                if (Keyboard.IsKeyDown(Key.F) && player1.IsReload == 0)
                    Shooting(player1);

                if (player1.IsReload > 0)
                    player1.IsReload--;

                //Стрельа второго игрока
                if (Keyboard.IsKeyDown(Key.RightCtrl) && player2.IsReload == 0)
                    Shooting(player2);

                if (player2.IsReload > 0)
                    player2.IsReload--;

                //Жив ли первый игрок
                if (player1.isAlive && player1.Health == 0)
                {
                    player1.isAlive = false;
                }

                //Жив ли второй игрок
                if (player2.isAlive && player2.Health == 0)
                {
                    player2.isAlive = false;
                }
            }      
        }

        //Стрельба, создание пуль
        private void Shooting(Ship pl)
        {
            if (pl.weaponsMode)
            {
                int direction = pl.direction == 2 || pl.direction == 4 ? 1 : 2;
                if (direction == 1)
                {
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X, pl.position.Y - 5), 1));
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X, pl.position.Y - 25), 1));
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X, pl.position.Y + 15), 1));
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X, pl.position.Y - 5), 3));
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X, pl.position.Y - 25), 3));
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X, pl.position.Y + 15), 3));
                }
                else
                {
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X - 5, pl.position.Y), 2));
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X - 25, pl.position.Y), 2));
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X + 15, pl.position.Y), 2));
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X - 5, pl.position.Y), 4));
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X - 25, pl.position.Y), 4));
                    pl.bullets.Add(new Bullet(new Vector2(pl.position.X + 15, pl.position.Y), 4));
                }
            }else
                pl.bullets.Add(new Bullet(new Vector2(pl.position.X, pl.position.Y), pl.direction));
            pl.IsReload = pl.getWeaponsReload();
        }

        //Движение пуль
        private void BulletsMove(Ship pl, Ship plEnemy)
        {
            for (int i = 0; i < pl.bullets.Count; i++)
            {
                Bullet bullet = pl.bullets[i];
                RenderTarget2D.DrawBitmap(bulletBmp, new RectangleF(bullet.position.X, bullet.position.Y, 10, 10), 1.0f, BitmapInterpolationMode.Linear);
                switch (bullet.direction)
                {
                    case 1:
                        bullet.position.X -= pl.getSpeedBullet();
                        break;
                    case 2:
                        bullet.position.Y -= pl.getSpeedBullet();
                        break;
                    case 3:
                        bullet.position.X += pl.getSpeedBullet();
                        break;
                    case 4:
                        bullet.position.Y += pl.getSpeedBullet();
                        break;
                }

                if (!borderCheck(bullet.position))
                {
                    pl.bullets.RemoveAt(i);
                    i--;
                }
                    
                foreach(Rock rock in rocks)
                {
                    if (Features.CollisionRect((int)bullet.position.X, (int)bullet.position.Y, (int)rock.position.X, (int)rock.position.Y, 10, 10, rock.scale, rock.scale))
                    {
                        pl.bullets.RemoveAt(i);
                        i--;
                    }
                }
                
                if (CollisionBullet(plEnemy, bullet))
                {
                    if(plEnemy.isAlive)
                        if(pl.weaponsMode)
                            plEnemy.takeDamage((int)(pl.getDamage() - plEnemy.getDamageAbsorption() * pl.getDamage()));
                        else
                            plEnemy.takeDamage((int)((pl.getDamage() - plEnemy.getDamageAbsorption() * pl.getDamage())*1.5f));
                    pl.bullets.RemoveAt(i);
                    i--;
                }
            }
        }
    }
}
